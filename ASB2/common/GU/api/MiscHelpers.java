package GU.api;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;
import ASB2.utils.UtilDirection;
import ASB2.vector.Vector3;
import GU.api.network.IConductor;
import GU.api.network.INetwork;

public class MiscHelpers {

    public static void addConductorsAround(Vector3 tile, World world, INetwork network) {

        MiscHelpers.addConductorsAround(tile.intX(), tile.intY(), tile.intZ(), world, network);
    }

    public static void addConductorsAround(TileEntity tile, World world, INetwork network) {

        MiscHelpers.addConductorsAround(tile.xCoord, tile.yCoord, tile.zCoord, world, network);
    }

    public static void addConductorsAround(int x, int y, int z, World world, INetwork network) {

        for(ForgeDirection direction : ForgeDirection.VALID_DIRECTIONS) {

            TileEntity tile = UtilDirection.translateDirectionToTile(world, direction,  x,  y, z);

            if(tile != null && tile instanceof IConductor) {

                if(((IConductor)tile).getNetwork() != null) {

                    if(((IConductor)tile).getNetwork() != network) {

                        ((IConductor)tile).getNetwork().mergeNetworks(world, network);
                    }
                }
                else {

                    ((IConductor)tile).setNetwork(network);
                }               
            }
        }
    }
}
