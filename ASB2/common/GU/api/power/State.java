package GU.api.power;

public enum State {

    SOURCE,
    SINK,
    OTHER;
}
