package GU.api.power;

public interface IPowerMisc {

    /**
     * Returns power provider class of the tile
     */
    IPowerProvider getPowerProvider();
}
