package GU.info;

import net.minecraft.util.ResourceLocation;

public class Gui {

    public static final int CREATION_TABLE = 0;
    public static final int SPEEDY_FURNACE = 1;
    public static final int BLOCK_BREAKER = 2;
    public static final int CAMO_BLOCK = 3;
    public static final int ADVANCED_POTION_BREWERY= 4;
    public static final int SENDER = 5;
    public static final int SOLAR_FOCUS = 6;
    public static final int MASHER = 7;
    public static final int DISSOLVER = 8;
    
    public static final ResourceLocation GUI_SMALL_BLANK = new ResourceLocation(Reference.MODDID + ":textures/blank/guiblank.png");
    public static final ResourceLocation GUI_DEFAULT = new ResourceLocation(Reference.MODDID + ":textures/blank/defaultGui.png");
}
