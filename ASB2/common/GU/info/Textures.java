package GU.info;

import net.minecraft.util.ResourceLocation;

public final class Textures {

    public static final ResourceLocation BLANK = new ResourceLocation(Reference.MODDID + ":textures/blocks/GearBlock.png");
    public static final ResourceLocation BLACK = new ResourceLocation(Reference.MODDID + ":textures/unused/ColoredBlack.png");
    public static final ResourceLocation TEST = new ResourceLocation(Reference.MODDID + ":textures/blocks/Colored.png");

    public static final ResourceLocation TEST_LASER = new ResourceLocation(Reference.MODDID + ":textures/blank/ModelLaser.png");
    public static final ResourceLocation TEST_LASER2 = new ResourceLocation(Reference.MODDID + ":textures/blank/ModelLaser2.png");

    public static final ResourceLocation LAMP = new ResourceLocation(Reference.MODDID + ":textures/blocks/ModelLamp.png");
    public static final ResourceLocation CREATION_TABLE = new ResourceLocation(Reference.MODDID + ":textures/models/ModelCreationTable2.png");
    public static final ResourceLocation BLOCK_BREAKER = new ResourceLocation(Reference.MODDID + ":textures/models/ModelBlockBreaker.png");
    public static final ResourceLocation RUNIC_CUBE = new ResourceLocation(Reference.MODDID + ":textures/models/ModelRunicCube.png");
    public static final ResourceLocation CLUSTER_SENDER = new ResourceLocation(Reference.MODDID + ":textures/models/ModelClusterSender/ModelClusterSender.png");

    public static final ResourceLocation MULTI_PANEL = new ResourceLocation(Reference.MODDID + ":textures/models/ModelMultiPanel/ModelMultiPanel.png");
    public static final ResourceLocation MULTI_PANEL_ADDITION_1 = new ResourceLocation(Reference.MODDID + ":textures/models/ModelMultiPanel/ModelMultiPanelRenderAddition1.png");
    public static final ResourceLocation MULTI_PANEL_ADDITION_2 = new ResourceLocation(Reference.MODDID + ":textures/models/ModelMultiPanel/ModelMultiPanelRenderAddition2.png");
    public static final ResourceLocation MULTI_PANEL_ADDITION_3 = new ResourceLocation(Reference.MODDID + ":textures/models/ModelMultiPanel/ModelMultiPanelRenderAddition3.png");
    public static final ResourceLocation MULTI_PANEL_ADDITION_4 = new ResourceLocation(Reference.MODDID + ":textures/models/ModelMultiPanel/ModelMultiPanelRenderAddition4.png");
    public static final ResourceLocation MULTI_PANEL_ADDITION_5 = new ResourceLocation(Reference.MODDID + ":textures/models/ModelMultiPanel/ModelMultiPanelRenderAddition5.png");
    public static final ResourceLocation MULTI_PANEL_ADDITION_6 = new ResourceLocation(Reference.MODDID + ":textures/models/ModelMultiPanel/ModelMultiPanelRenderAddition6.png");
    public static final ResourceLocation MULTI_PANEL_ADDITION_7 = new ResourceLocation(Reference.MODDID + ":textures/models/ModelMultiPanel/ModelMultiPanelRenderAddition7.png");

    public static final ResourceLocation BLOOD_STONE_CUBE = new ResourceLocation(Reference.MODDID + ":textures/models/ModelBloodStone/ModelBloodStoneCube.png");
    public static final ResourceLocation BLOOD_STONE_HEXAGON = new ResourceLocation(Reference.MODDID + ":textures/models/ModelBloodStone/ModelBloodStoneHexagon.png");

    public static final ResourceLocation SOLAR_FOCUS_TOP = new ResourceLocation(Reference.MODDID + ":textures/models/ModelSolarFocus/ModelSolarFocusTop.png");
    public static final ResourceLocation SOLAR_FOCUS_BOTTOM = new ResourceLocation(Reference.MODDID + ":textures/models/ModelSolarFocus/ModelSolarFocusBottom.png");

    public static final ResourceLocation UNIVERSAL_CONDUIT = new ResourceLocation(Reference.MODDID + ":textures/models/ModelUniversalConduit/ModelUniversalConduit.png");
    public static final ResourceLocation UNIVERSAL_CONDUIT_CENTER = new ResourceLocation(Reference.MODDID + ":textures/models/ModelUniversalConduit/ModelUniversalConduitCenter.png");

    public static final ResourceLocation CONDUIT_INTERFACE_CENTER = new ResourceLocation(Reference.MODDID + ":textures/models/ModelConduitInterface/ModelConduitInterfaceCenter.png");
    public static final ResourceLocation CONDUIT_INTERFACE_IMPORTING = new ResourceLocation(Reference.MODDID + ":textures/models/ModelConduitInterface/ModelConduitInterfaceImporting.png");
    public static final ResourceLocation CONDUIT_INTERFACE_EXPORTING = new ResourceLocation(Reference.MODDID + ":textures/models/ModelConduitInterface/ModelConduitInterfaceExporting.png");

    public static final ResourceLocation ENERGY_CUBE_CENTER = new ResourceLocation(Reference.MODDID + ":textures/models/ModelEnergyCube/ModelEnergyCubeCenter.png");
    public static final ResourceLocation ENERGY_CUBE_UP = new ResourceLocation(Reference.MODDID + ":textures/models/ModelEnergyCube/ModelEnergyCubeUp.png");
    public static final ResourceLocation ENERGY_CUBE_DOWN = new ResourceLocation(Reference.MODDID + ":textures/models/ModelEnergyCube/ModelEnergyCubeDown.png");
    public static final ResourceLocation ENERGY_CUBE_NORTH = new ResourceLocation(Reference.MODDID + ":textures/models/ModelEnergyCube/ModelEnergyCubeNorth.png");
    public static final ResourceLocation ENERGY_CUBE_SOUTH = new ResourceLocation(Reference.MODDID + ":textures/models/ModelEnergyCube/ModelEnergyCubeSouth.png");
    public static final ResourceLocation ENERGY_CUBE_EAST = new ResourceLocation(Reference.MODDID + ":textures/models/ModelEnergyCube/ModelEnergyCubeEast.png");
    public static final ResourceLocation ENERGY_CUBE_WEST = new ResourceLocation(Reference.MODDID + ":textures/models/ModelEnergyCube/ModelEnergyCubeWest.png");

    public static final ResourceLocation ENERGY_CUBE_CENTER_EXPORTING = new ResourceLocation(Reference.MODDID + ":textures/models/ModelEnergyCube/exporting/ModelEnergyCubeCenter.png");
    public static final ResourceLocation ENERGY_CUBE_UP_EXPORTING = new ResourceLocation(Reference.MODDID + ":textures/models/ModelEnergyCube/exporting/ModelEnergyCubeUp.png");
    public static final ResourceLocation ENERGY_CUBE_DOWN_EXPORTING = new ResourceLocation(Reference.MODDID + ":textures/models/ModelEnergyCube/exporting/ModelEnergyCubeDown.png");
    public static final ResourceLocation ENERGY_CUBE_NORTH_EXPORTING = new ResourceLocation(Reference.MODDID + ":textures/models/ModelEnergyCube/exporting/ModelEnergyCubeNorth.png");
    public static final ResourceLocation ENERGY_CUBE_SOUTH_EXPORTING = new ResourceLocation(Reference.MODDID + ":textures/models/ModelEnergyCube/exporting/ModelEnergyCubeSouth.png");
    public static final ResourceLocation ENERGY_CUBE_EAST_EXPORTING = new ResourceLocation(Reference.MODDID + ":textures/models/ModelEnergyCube/exporting/ModelEnergyCubeEast.png");
    public static final ResourceLocation ENERGY_CUBE_WEST_EXPORTING = new ResourceLocation(Reference.MODDID + ":textures/models/ModelEnergyCube/exporting/ModelEnergyCubeWest.png");

    public static final ResourceLocation HANDHELD_TANK_CENTER = new ResourceLocation(Reference.MODDID + ":textures/models/ModelHandheldTank/ModelHandheldTankCenter.png");
    public static final ResourceLocation HANDHELD_TANK_OUTSIDE = new ResourceLocation(Reference.MODDID + ":textures/models/ModelHandheldTank/ModelHandheldTankOutside.png");

    public static final ResourceLocation SOLAR_GYRO_BASE = new ResourceLocation(Reference.MODDID + ":textures/models/ModelSolarGyro/ModelSolarGyroBase.png");
    public static final ResourceLocation SOLAR_GYRO_CENTER = new ResourceLocation(Reference.MODDID + ":textures/models/ModelSolarGryo/ModelSolarGyroCenter.png");
    public static final ResourceLocation SOLAR_GYRO_OUTER = new ResourceLocation(Reference.MODDID + ":textures/models/ModelSolarGyro/ModelSolarGyroOuter.png");

    public static final ResourceLocation STEAM_GYRO_BASE = new ResourceLocation(Reference.MODDID + ":textures/models/ModelSteamGyro/ModelSteamGyroBase.png");
    public static final ResourceLocation STEAM_GYRO_CENTER = new ResourceLocation(Reference.MODDID + ":textures/models/ModelSteamGyro/ModelSteamGyroCenter.png");
    public static final ResourceLocation STEAM_GYRO_OUTER = new ResourceLocation(Reference.MODDID + ":textures/models/ModelSteamGyro/ModelSteamGyroOuter.png");

    public static final ResourceLocation GLASS_PIPE_CENTER = new ResourceLocation(Reference.MODDID + ":textures/models/ModelGlassPipe/ModelGlassPipeCenter.png");
    public static final ResourceLocation GLASS_PIPE_NORTH = new ResourceLocation(Reference.MODDID + ":textures/models/ModelGlassPipe/ModelGlassPipeWireNorth.png");
    public static final ResourceLocation GLASS_PIPE_SOUTH = new ResourceLocation(Reference.MODDID + ":textures/models/ModelGlassPipe/ModelGlassPipeWireSouth.png");
    public static final ResourceLocation GLASS_PIPE_WEST = new ResourceLocation(Reference.MODDID + ":textures/models/ModelGlassPipe/ModelGlassPipeWireWest.png");
    public static final ResourceLocation GLASS_PIPE_EAST = new ResourceLocation(Reference.MODDID + ":textures/models/ModelGlassPipe/ModelGlassPipeWireeEast.png");
    public static final ResourceLocation GLASS_PIPE_UP = new ResourceLocation(Reference.MODDID + ":textures/models/ModelGlassPipe/ModelGlassPipeWireUp.png");
    public static final ResourceLocation GLASS_PIPE_DOWN = new ResourceLocation(Reference.MODDID + ":textures/models/ModelGlassPipe/ModelGlassPipeWireDown.png");

    public static final ResourceLocation CRYSTAL_BLANK = new ResourceLocation(Reference.MODDID + ":textures/items/ModelCrystalBlank.png");
    public static final ResourceLocation CRYSTAL_AIR = new ResourceLocation(Reference.MODDID + ":textures/items/ModelCrystalAir.png");
    public static final ResourceLocation CRYSTAL_EARTH = new ResourceLocation(Reference.MODDID + ":textures/items/ModelCrystalEarth.png");
    public static final ResourceLocation CRYSTAL_FIRE = new ResourceLocation(Reference.MODDID + ":textures/items/ModelCrystalFire.png");
    public static final ResourceLocation CRYSTAL_WATER = new ResourceLocation(Reference.MODDID + ":textures/items/ModelCrystalWater.png");
    public static final ResourceLocation CRYSTAL_ENERGY = new ResourceLocation(Reference.MODDID + ":textures/items/ModelCrystalEnergy.png");
}
